from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }

    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)


def update_todo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "todo_object": todo,
        "form": form,
            }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }

    return render(request, "todos/create_items.html", context)


def update_todo_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "todo_item": todo_item,
        "form": form,
    }

    return render(request, "todos/edit_items.html", context)
